<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="keywords" content="">
		<title>Famil & Acácia - Salto/SP</title>
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/main.min.css" rel="stylesheet">
		<link href="http://fonts.googleapis.com/css?family=Ubuntu:300,500,300italic,500italic" rel="stylesheet">
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body id="top" data-spy="scroll" data-target=".main-header-navbar" data-offset="85">
		<div class="main-alerts">
			<!--[if lt IE 9]>
			<div class="alert alert-danger alert-dismissible" role="alert">
				<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> 
				<span class="sr-only">Error:</span>
				O seu navegador está muito desatualizado, por favor, atualize seu navegador para melhorar sua experiência. 
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<![endif]-->
		</div>
		
		<header class="main-header navbar navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".main-header-navbar-collapse">
						<span class="sr-only">Alternar navegação</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#top" data-smooth-scroll><img src="img/logo-famil-acacia.png" width="356" height="60" alt="Logotipo Famil & Acácia"></a>
				</div>
				<nav class="main-header-navbar-collapse navbar-collapse collapse" aria-expanded="false">
					<ul class="nav navbar-nav navbar-right">
						<li><a href="#sobre" data-smooth-scroll>Quem Somos</a></li>
						<li><a href="#qualidade" data-smooth-scroll>Qualidade</a></li>
						<li><a href="#produtos" data-smooth-scroll>Produtos</a></li>
						<li><a href="#uniformes" data-smooth-scroll>Uniformes Exclusivos</a></li>
						<li><a href="#contato" data-smooth-scroll>Contato</a></li>
					</ul>
				</nav>
			</div>
		</header>
		
		<section class="main-hero" style="background-image: url(img/hero-01-bg.jpg);">
			<div class="main-hero-inner">
				<div class="container">
					<div class="row">
						<div class="col-a col-md-7 col-md-push-5 text-center-xs text-center-sm">
							<div class="main-hero-content">
								<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="3000" data-pause="false">
									<div class="carousel-inner" role="listbox">
										<div class="item active">
											<h1 class="heading heading-lg">Uniformes <br class="hidden-xs hidden-sm">profissionais</h1>
											<p>Sua empresa vista com mais elegância.</p>
										</div>
										<div class="item">
											<h1 class="heading heading-lg">Qualidade <br class="hidden-xs hidden-sm">nos detalhes</h1>
											<p>Uniformes com durabilidade e conforto.</p>
										</div>
										<div class="item">
											<h1 class="heading heading-lg">Uniformes <br class="hidden-xs hidden-sm">exclusivos</h1>
											<p>Teremos muito prazer em atendê-lo.</p>
										</div>
									</div>
								</div>								
							</div>
						</div>
						<div class="col-b col-md-5 col-md-pull-7 text-center-xs text-center-sm">
							<img class="img-responsive" src="img/hero-01.png" width="424" height="671">	
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="main-section text-center" id="sobre">
			<div class="container">
				<h1 class="heading heading-lg">Quem Somos</h1>
				<div class="spacing spacing-1"></div>
				<div class="spacing spacing-3 spacing-bordered-top"></div>
				<p>Criada em 1998, a Famil confecciona uniformes profissionais e industriais personalizados de acordo com as necessidades da sua empresa. Nosso objetivo é fornecer uniformes com qualidade e a preço justo para que sua empresa seja bem vista no mercado em que atua. A Famil confecciona uniformes para diversos setores seguindo rígidos padrões de qualidade, durabilidade e resistência. Fale conosco para mais informações.</p>
				<p>A Famil & Acácia atende clientes de pequeno, médio e grande porte, sem quantidade mínima de peças.</p>
			</div>
			<div class="spacing spacing-3"></div>
			<img class="img-responsive" src="img/sobre.jpg" width="1123" height="386">
		</section>

		<section class="main-section main-section-gray" id="qualidade">
			<div class="container">
				<div class="row">
					<div class="col-a col-md-5 text-center">
						<img class="img-responsive" src="img/qualidade.jpg" width="300" height="324">
					</div>
					<div class="col-b col-md-7 text-center-xs text-center-sm">
						<div class="spacing-1 visible-md visible-lg"></div>
						<h1 class="heading heading-lg">Qualidade</h1>
						<div class="spacing spacing-1"></div>
						<div class="spacing spacing-3 spacing-bordered-top visible-xs visible-sm"></div>
						<p>A Famil & Acácia preza pela qualidade de seus produtos, desenvolvendo peças com alto padrão de qualidade. Desde a escolha do tecido, costuras e acabamento a Famil & Acácia tem um know-how de mais de 15 anos de experiência, que permite oferecer uniformes com o melhor custo x benefício do mercado. Através de colaboradores experientes e com dedicação e empenho diário a Famil & Acácia oferece mais que uniformes, oferece soluções para os colaboradores da sua empresa, visando qualidade, conforto e segurança, além de valorizar a identidade corporativa da sua empresa.</p>
						<p>A Famil & Acácia atende pedidos de pequeno, médio e grande volume.</p>
						<p>
							<img src="img/selo-qualidade-em-atendimento.jpg" width="88" height="140" alt="Selo Qualidade em Atendimento">
							<img src="img/selo-empresa-ouro.jpg" width="148" height="140" alt="Selo Empresa Ouro">
						</p>
					</div>
				</div>
			</div>
		</section>

		<section class="main-section text-center" id="produtos">
			<div class="container">
				<h1 class="heading heading-lg">O que fazemos</h1>
				<div class="spacing spacing-1"></div>
				<div class="spacing spacing-3 spacing-bordered-top"></div>
				<p>Conheça abaixo os tipos de uniformes feitos pela Famil & Acácia. Fale conosco para mais informações.</p>
				<div class="spacing spacing-2"></div>
				<div class="row" data-match-col>
					<div class="col-sm-6 col-md-4">
						<img class="img-responsive" src="img/uniformes-operacionais.jpg" width="300" height="300" alt="Uniformes Operacionais">
						<h3 class="heading">Operacional</h3>
						<p>Atividades operacionais em todos os segmentos industriais requerem um maior esforço físico, por isso a Famil & Acácia oferece produtos que priorizam a alta resistência e conforto ao usuário.</p>
						<div class="spacing spacing-2"></div>
					</div>
					<div class="col-sm-6 col-md-4">
						<img class="img-responsive" src="img/uniformes-administrativos.jpg" width="300" height="300" alt="Uniformes Administrativos">
						<h3 class="heading">Administrativo</h3>
						<p>Esses profissionais precisam de uniformes que valorizem a boa apresentação pessoal e a marca da empresa, por isso a Famil & Acácia oferece tecidos leves, com ótimo caimento e menos amarrotamento.</p>
						<div class="spacing spacing-2"></div>
					</div>
					<div class="col-sm-6 col-md-4">
						<img class="img-responsive" src="img/uniformes-servicos.jpg" width="300" height="300" alt="Uniformes para Serviços">
						<h3 class="heading">Serviços</h3>
						<p>A versatilidade é a principal necessidade dos profissionais do setor de prestação de serviços. Os tecidos devem permitir a realização de atividades ao ar livre e possibilitar boa apresentação pública.</p>
						<div class="spacing spacing-2"></div>
					</div>
					<div class="col-sm-6 col-md-4">
						<img class="img-responsive" src="img/uniformes-saude.jpg" width="300" height="300" alt="Uniformes para Saúde">
						<h3 class="heading">Saúde/Hospitalar</h3>
						<p>O ambiente hospitalar requer tecidos mais leves e de fácil limpeza, para evitar contaminação. Segurança em primeiro lugar para o profissional e o paciente.</p>
						<div class="spacing spacing-2"></div>
					</div>
					<div class="col-sm-6 col-md-4">
						<img class="img-responsive" src="img/uniformes-alimentacao.jpg" width="300" height="300" alt="Uniformes para Alimentação">
						<h3 class="heading">Alimentação</h3>
						<p>As áreas de alimentação requerem um ambiente limpo, que transmita ao público qualidade e confiabilidade, e ao mesmo tempo valorize a imagem corporativa.</p>
						<div class="spacing spacing-2"></div>
					</div>
					<div class="col-sm-6 col-md-4">
						<img class="img-responsive" src="img/uniformes-seguranca.jpg" width="300" height="300" alt="Uniformes para Segurança">
						<h3 class="heading">Segurança</h3>
						<p>Os fardamentos militares e de segurança particular devem garantir a proteção aos profissionais, sobretudo mediante a grande esforço físico.</p>
						<div class="spacing spacing-2"></div>
					</div>
				</div>
				<h4>Consulte também nossas camisas polo masculinas/femininas e camisetas tradicionais em malha.</h4>
				<div class="spacing spacing-2"></div>
				<img class="img-responsive" src="img/uniformes-camisas.jpg" width="515" height="164" alt="Camisas Polo e Tradicionais">
				<div class="spacing spacing-1"></div>
			</div>
		</section>

		<section class="main-section main-section-gray" id="uniformes">
			<div class="container">
				<div class="row">
					<div class="col-a col-md-6 col-md-push-6 text-center-xs text-center-sm">
						<div class="spacing spacing-10 visible-md visible-lg"></div>
						<div class="spacing spacing-10 visible-md visible-lg"></div>
						<h1 class="heading heading-lg">Uniformes Exclusivos</h1>
						<div class="spacing spacing-1"></div>
						<div class="spacing spacing-3 spacing-bordered-top visible-xs visible-sm"></div>
						<p>Mais um diferencial são os modelos ideais para cada cliente, criados especialmente para cada tipo de empresa, de acordo com a função, com o mercado e o ramo de atividade da empresa. Além do modelo, a equipe indica o tecido ideal para cada necessidade.</p>
					</div>
					<div class="col-b col-md-6 col-md-pull-6 text-center">
						<div class="spacing spacing-1 visible-xs visible-sm"></div>
						<img class="img-responsive" src="img/uniformes.jpg" width="340" height="620">
					</div>
				</div>
			</div>
		</section>

		<section class="main-section text-center" id="contato">
			<div class="container">
				<h1 class="heading heading-lg">Fale Conosco</h1>
				<div class="spacing spacing-1"></div>
				<div class="spacing spacing-3 spacing-bordered-top"></div>
				<p>Entre em contato conosco para esclarecer qualquer dúvida. O nosso maior prazer é atendê-lo sempre da melhor forma.</p>
				<div class="spacing spacing-1"></div>
				<p class="h4 text-uppercase">Fornecemos pedidos de pequeno, médio e grande volume.</p>
				<p>Sem quantidade mínima de peças. Fale conosco.</p>
				<div class="spacing spacing-1"></div>
				<p class="h4"><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span> 11 4029-2542</p>
				<div class="spacing spacing-3"></div>
				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						<form id="contactForm" action="contact.php">
							<div class="row">
								<div class="col-md-6 form-group text-center">
									<label class="sr-only" for="contactFormName">Seu nome</label>
									<input class="form-control input-lg" id="contactFormName" type="text" name="ContactForm[name]" placeholder="Seu nome">
								</div>
								<div class="col-md-6 form-group text-center-xs text-center-sm">
									<label class="sr-only" for="contactFromEmail">Seu e-mail</label>
									<input class="form-control input-lg" id="contactFromEmail" type="text" name="ContactForm[email]" placeholder="Seu e-mail">
								</div>
							</div>
							<div class="spacing spacing-1 visible-md visible-lg"></div>
							<div class="form-group">
								<label class="sr-only" for="contactFromMessage">Mensagem</label>
								<textarea class="form-control input-lg" id="contactFromMessage" name="ContactForm[message]" placeholder="Mensagem"></textarea>
							</div>
							<div class="spacing spacing-2"></div>
							<div class="form-group">
								<button class="btn btn-lg btn-primary" type="submit">Enviar Mensagem</button><span class="divider hidden-xs hidden-md"></span> <div class="spacing spacing-2 hidden-md hidden-lg"></div> Ou envie para <a href="maito:contato@familuniformes.com.br">contato@familuniformes.com.br</a>.
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="spacing spacing-6"></div>
			<div class="main-map"> 
				<div id="map" class="main-map-map"></div>
				<address class="main-map-address">
					<span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> Rua Itália, 511 - Jd. Elizabeth - Salto /SP
				</address>
			</div>
		</section>

		<footer class="main-footer">
			<div class="container">
				Copyright &copy <?php echo date('Y'); ?>. Famil & Acácia. Todos os direitos reservados.
			</div>
		</footer>

		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/main.js"></script>
	</body>
</html>