!function($) {

	'use strict';


 	/* Misc */ 

	$(document).ready(function() {
		setTimeout(function() {
			adjust();
		}, 1000);
		setTimeout(function() {
			loadGoogleMaps();
		}, 2000);
	});

	$(window).on('resize', function() {
		adjust();
	});


	/* Layout */ 

	function adjust()
	{
		matchCols();
		$('body').css('padding-top', function() {
			return $('.main-header').outerHeight();
		});
	}	

	function matchCols() {
		var rows = $('[data-match-col]');
		$(rows).each(function(index, row) {
			var cols = $(row).find('[class^="col-"]');
			var height = 0;
			$(cols).each(function() {
				$(this).css('height', 'auto');
				if ($(this).outerHeight() > height) {
					height = $(this).outerHeight();
				}
			});
			$(cols).css('height', height);
		});
	}


 	/* Scroll */ 

	$('[data-smooth-scroll]').on('click', function(e) { 
		e.preventDefault();
		var target = $(this).attr('href');
		var $target = $(target);
		$('html, body').stop().animate({
			'scrollTop': ($target.offset().top - $('.main-header').outerHeight())
		}, 500);
	});


 	/* Contact */ 

	$('#contactForm').on('submit', function(e) {
		e.preventDefault();
		submitContact($(this));
		return false;
	}); 

	function submitContact($form) {
		var $request = $.ajax({
			type: 'POST',
			url: $form.attr('action'),
			dataType: 'html',
			data: $form.serialize(),
			beforeSend: function() { 
				$form.addClass('loading') 
				$form.find('.alert').remove();
			},
		});

		$request.done(function(data) {
			$form.prepend(data);
		});

		$request.fail(function(jqXHR) {
			$form.prepend(jqXHR.responseText);
		});

		$request.always(function(data) {
			$form.removeClass('loading');
		});

		return false; 
	}

}(window.jQuery);   


/* Map */ 

function loadGoogleMaps() {
	var script = document.createElement('script');
	script.type = 'text/javascript';
	script.src = '//maps.googleapis.com/maps/api/js?v=3.exp&callback=initializeGoogleMaps';
	document.body.appendChild(script);
}

function initializeGoogleMaps() {
	var styles = [
		{
			stylers: [
				{ saturation: -100 }
			]
		},{
			featureType: 'road',
			elementType: 'geometry',
			stylers: [
				{ lightness: 10 },
				{ visibility: 'simplified' }
			]
		},{
			featureType: 'road',
			elementType: 'labels',
			stylers: [
				{ visibility: 'off' }
			]
		}
	];

	var styledMap = new google.maps.StyledMapType(styles, { name: 'Styled Map' });

	var mapOptions = {
		zoom: 15,
		scrollwheel: false,
		disableDefaultUI: true,
		zoomControl: true,
		streetViewControl: true,
		center: new google.maps.LatLng(-23.1954038,-47.3055286),
		mapTypeControlOptions: {
			mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'mapStyle']
		}
	};

	var map = new google.maps.Map(document.getElementById('map'), mapOptions);

	map.mapTypes.set('mapStyle', styledMap);
	map.setMapTypeId('mapStyle');

	var marker = new google.maps.Marker({
		position: new google.maps.LatLng(-23.1954038,-47.3055286),
		map: map,
		title: 'Layout',
		animation: google.maps.Animation.DROP
	});
}
