<?php

// --------------------------------------
// CONFIGURAÇÃO
// --------------------------------------

$smtpHost       = '';
$smtpPort       = '';
$smtpUsername   = '';
$smtpPassword   = '';
$smtpEncryption = 'tls'; 
$senderEmail    = 'contato@familuniformes.com.br';

// --------------------------------------

$error   = $success = '';
$post    = isset($_POST['ContactForm']) ? $_POST['ContactForm'] : array();

$name = isset($post['name']) ? $post['name'] : null;
$email = isset($post['email']) ? $post['email'] : null;
$message = isset($post['message']) ? $post['message'] : null;

if (!$email) $error = 'Por favor, preencha seu e-mail.';
if (!$message) $error = 'Por favor, preencha a mensagem.';

if (!$error) {
	$body = <<<HTML
<p>Olá, $email enviou uma mensagem através do site.</p>
<table>
	<tbody>
		<tr>
			<th align="right">Nome:</th>
			<td>$name</td>
		</tr>
		<tr>
			<th align="right">E-mail:</th>
			<td>$email</td>
		</tr>
		<tr>
			<th align="right">Mensagem:</th>
			<td>$message</td>
		</tr>
	</tbody>
</table>
HTML;


	try {
		// Import Swift library

		require_once 'vendor' . DIRECTORY_SEPARATOR . 'swiftmailer' . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'swift_required.php';

		// Create the Transport

		if ($smtpHost) {
			$mailTransport = Swift_SmtpTransport::newInstance()
				->setHost($smtpHost)
				->setPort($smtpPort)
				->setEncryption($smtpEncryption)
				->setUsername($smtpUsername)
				->setPassword($smtpPassword);
		} else
			$mailTransport = Swift_MailTransport::newInstance();

		// Create the Mailer using the created Transport

		$mailer = Swift_Mailer::newInstance($mailTransport);

		// Create a message

		$mailMessage = Swift_Message::newInstance()
			->setSubject('Site > Contato')
			->setFrom($senderEmail)
			->setTo($senderEmail)
			->setReplyTo(array($email => $name))
			->setBody($body, 'text/html');

		// Send the message

		$mailer->send($mailMessage);
		$success = 'Obrigado! Retornaremos o seu contato o mais breve possível.';
	} catch (Exception $e) {
		$error = 'Não foi possível enviar a mensagem. Por favor, tente novamente mais tarde. ' . get_class($e) . ': ' . $e->getMessage();
	}
}

/*
if ($error !== '')
	header("HTTP/1.0 400");
*/
?>

<?php if ($error) : ?>

<div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
  <?php echo $error; ?>
</div>

<?php else : ?>
	
<div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
  <?php echo $success; ?>
</div>

<?php endif; ?>